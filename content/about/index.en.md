---
title: "About this website"
date: 2021-06-19T18:00:00+00:00
draft: false

lightgallery: false

math:
  enable: false
---

### About Outmo.de

Things often start with inspirational quotes, like this one from [Buckminster Fuller](https://en.wikipedia.org/wiki/Buckminster_Fuller):

> You never change things by fighting the existing reality. To change something, build a new model that makes the existing model obsolete.

And building something starts with imagining it, which more often then not is first done in works of fiction.

Outmode is an informal writers' collective that operates on an invite only basis.

#### Technical basis

Our collective hosts a wide range of cloud software for members to use, but besides this website most of the public stories can be found on our ActivityPub enabled [WriteFreely](https://writefreely.org/) instance [here](https://scifi.outmo.de) where you can follow our authors via the [Fediverse](https://fediverse.party). 

#### Financial model

Making a living out of writing is not the primary goal, but we hope that by pooling our works we can convince more people to start supporting us  through recurring-donation platforms such as LiberaPay or Patreon.

Becoming a member is free of cost, but of course donations from members to keep the infrastructure running are still apprechiated.
