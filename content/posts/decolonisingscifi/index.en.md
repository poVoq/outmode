---
title: "Decolonising Sci-Fi"
date: 2021-06-19T17:00:00+00:00
lastmod: 2021-06-18T17:00:00+00:00
draft: false
author: "Outmode"
authorLink: "http://outmo.de"
description: "Exploring space without settler colonizing, coding alien/fantasy races without racism, ditching tired Yellow Peril cyberpunk tropes for counter-hegemonic cooperation."
#resources:
#- name: "featured-image"
#  src: "featured-image.png"

tags: ["scifi", "writing", "video"]
categories: ["Writing advise"]

lightgallery: false
---

### Decolonising Sci-Fi

{{< youtube LZa8pJhU2MM >}}

#### tl;dr

Exploring space without settler colonizing, coding alien/fantasy races without racism, ditching tired Yellow Peril cyberpunk tropes for counter-hegemonic cooperation.
