---
title: "Solarpunk Manifesto"
date: 2021-06-19T16:00:00+00:00
lastmod: 2021-06-18T16:00:00+00:00
draft: false
author: "ReDes"
authorLink: "http://www.re-des.org/"
description: "This Solarpunk Manifesto is a creative re-adaptation of ideas about solarpunk written by many people"
#resources:
#- name: "featured-image"
#  src: "featured-image.png"

tags: ["solarpunk", "meta"]
categories: ["Solarpunk"]

lightgallery: false
---
[This Solarpunk Manifesto](http://www.re-des.org/a-solarpunk-manifesto/) is a creative re-adaptation of ideas about solarpunk written by many people. These ideas can be mainly found in Solarpunk: a reference guide which can be found [here](https://medium.com/solarpunks/solarpunk-a-reference-guide-8bcf18871965) and in Solarpunk: Notes towards a Manifesto by Adam Flynn, which can be found [here](https://hieroglyph.asu.edu/2014/09/solarpunk-notes-toward-a-manifesto/).

<!--more-->

### A Solarpunk Manifesto

Solarpunk is a movement in speculative fiction, art, fashion, and activism that seeks to answer and embody the question “what does a sustainable civilization look like, and how can we get there?” 

The aesthetics of Solarpunk merge the practical with the beautiful, the well-designed with the green and lush, the bright and colorful with the earthy and solid. 

Solarpunk can be utopian, just optimistic, or concerned with the struggles en route to a better world ,  but never dystopian. As our world roils with calamity, we need solutions, not only warnings.

Solutions to thrive without fossil fuels, to equitably manage real scarcity and share in abundance instead of supporting false scarcity and false abundance, to be kinder to each other and to the planet we share.

Solarpunk is at once a vision of the future, a thoughtful provocation, a way of living and a set of achievable proposals to get there.

- We are solarpunks because optimism has been taken away from us and we are trying to take it back.
- We are solarpunks because the only other options are denial or despair.
- At its core, Solarpunk is a vision of a future that embodies the best of what humanity can achieve: a post-scarcity, post-hierarchy, post-capitalistic world where humanity sees itself as part of nature and clean energy replaces fossil fuels.
- The “punk” in Solarpunk is about rebellion, counterculture, post-capitalism, decolonialism and enthusiasm. It is about going in a different direction than the mainstream, which is increasingly going in a scary direction.
- Solarpunk is a movement as much as it is a genre: it is not just about the stories, it is also about how we can get there.
- Solarpunk embraces a diversity of tactics: there is no single right way to do solarpunk. Instead, diverse communities from around the world adopt the name and the ideas, and build little nests of self-sustaining revolution.
- Solarpunk provides a valuable new perspective, a paradigm and a vocabulary through which to describe one possible future. Instead of embracing retrofuturism, solarpunk looks completely to the future. Not an alternative future, but a possible future.
- Our futurism is not nihilistic like cyberpunk and it avoids steampunk’s potentially quasi-reactionary tendencies: it is about ingenuity, generativity, independence, and community.
- Solarpunk emphasizes environmental sustainability and social justice.
- Solarpunk is about finding ways to make life more wonderful for us right now, and also for the generations that follow us.
- Our future must involve repurposing and creating new things from what we already have. Imagine “smart cities” being junked in favor of smart citizenry.
- Solarpunk recognizes the historical influence politics and science fiction have had on each other.
- Solarpunk recognizes science fiction as not just entertainment but as a form of activism.
- Solarpunk wants to counter the scenarios of a dying earth, an insuperable gap between rich and poor, and a society controlled by corporations. Not in hundreds of years, but within reach.
- Solarpunk is about youth maker culture, local solutions, local energy grids, ways of creating autonomous functioning systems. It is about loving the world.
- Solarpunk culture includes all cultures, religions, abilities, sexes, genders and sexual identities.
- Solarpunk is the idea of humanity achieving a social evolution that embraces not just mere tolerance, but a more expansive compassion and acceptance.
- The visual aesthetics of Solarpunk are open and evolving. As it stands, it is a mash-up of the following:
    - 1800s age-of-sail/frontier living (but with more bicycles)
    - Creative reuse of existing infrastructure (sometimes post-apocalyptic, sometimes present-weird)
    - Appropriate technology
    - Art Nouveau
    - Hayao Miyazaki
    - Jugaad-style innovation from the non-Western world
    - High-tech backends with simple, elegant outputs 
- Solarpunk is set in a future built according to principles of New Urbanism or New Pedestrianism and environmental sustainability.
- Solarpunk envisions a built environment creatively adapted for solar gain, amongst other things, using different technologies. The objective is to promote self sufficiency and living within natural limits.
- In Solarpunk we’ve pulled back just in time to stop the slow destruction of our planet. We’ve learned to use science wisely, for the betterment of our life conditions as part of our planet. We’re no longer overlords. We’re caretakers. We’re gardeners.
- Solarpunk:
     - is diverse
     - has room for spirituality and science to coexist
     - is beautiful
     - can happen. **Now!**
