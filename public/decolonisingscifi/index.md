# Decolonising Sci-Fi


### Decolonising Sci-Fi

{{< youtube LZa8pJhU2MM >}}

#### tl;dr

Exploring space without settler colonizing, coding alien/fantasy races without racism, ditching tired Yellow Peril cyberpunk tropes for counter-hegemonic cooperation.

